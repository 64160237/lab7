package com.jtrps.week8;

public class TreeTestClass {
    private String name;
    private int x;
    private int y;

    public TreeTestClass(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public String getName() {
        return name;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void print() {
        System.out.println(name + " x: " + x + " y: " + y);
    }
}
