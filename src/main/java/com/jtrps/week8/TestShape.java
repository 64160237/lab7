package com.jtrps.week8;

public class TestShape {
    public static void main(String[] args) {
        RectangleShape rect1 = new RectangleShape("rect1", 10, 5);
        rect1.Area();
        rect1.Perimeter();
        rect1.print();

        RectangleShape rect2 = new RectangleShape("rect2", 5, 3);
        rect2.Area();
        rect2.Perimeter();
        rect2.print();

        CircleShape circle1 = new CircleShape("circle1", 1);
        circle1.Area();
        circle1.Girth();
        circle1.print();
        
        CircleShape circle2 = new CircleShape("circle2", 2);
        circle2.Area();
        circle2.Girth();
        circle2.print();

        TriangleShape triangle1 = new TriangleShape("triangle1", 5, 5, 6);
        triangle1.Area();
        triangle1.Perimeter();
        triangle1.print();
    }
}
