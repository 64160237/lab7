package com.jtrps.week8;

public class TestMap2 {
    public int width;
    public int height;

    public TestMap2(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public TestMap2() {
        this.width = 10;
        this.height = 10;
    }

    public void print() {
        System.out.println("MAP2");
        for (int i = 0; i < height; i++) { 
            for (int j = 0; j < width+1; j++) {
                if(j == width){
                    System.out.println();
                }else{
                    System.out.print("-");
                }
            }
        }
    }
}
