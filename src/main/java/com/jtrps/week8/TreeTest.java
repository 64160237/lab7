package com.jtrps.week8;

public class TreeTest {
    
     public static void main(String[] args) {
        TreeTestClass tree1 = new TreeTestClass("tree1", 5, 10);
        tree1.print(); 

        TreeTestClass tree2 = new TreeTestClass("tree2", 5, 11);
        tree2.print();
     }  
}
