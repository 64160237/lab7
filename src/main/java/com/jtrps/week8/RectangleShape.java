package com.jtrps.week8;

public class RectangleShape {
    private String name;
    private double width;
    private double height;

    public RectangleShape(String name, double width, double height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }
    
    public double Area() {
        double area = width * height;
        return area;
    }



    public double Perimeter() {
        double perimeter = (width + height) * 2;
        return perimeter;
    }

    public void print() {
        System.out.println(name + " Area = " + Area());
        System.out.println(name + " Perimeter = " + Perimeter());
    }
}
