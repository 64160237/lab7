package com.jtrps.week8;

public class TriangleShape {
    private String name;
    private double sideA;
    private double sideB;
    private double sideC;

    public TriangleShape(String name, double sideA, double sideB, double sideC) {
        this.name = name;
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public String getName() {
        return name;
    }
    
    public double Area() {
        double sumDTwo = (sideA + sideB + sideC)/2;
        double area = Math.sqrt(sumDTwo*(sumDTwo-sideA)*(sumDTwo-sideB)*(sumDTwo-sideC));
        return area;
    }

    public double Perimeter() {
        double perimeter = sideA + sideB + sideC;
        return perimeter;
    }

    public void print() {
        System.out.println(name + " Area = " + Area());
        System.out.println(name + " Perimeter = " + Perimeter());
    }
}
