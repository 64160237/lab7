package com.jtrps.week8;

public class CircleShape {
    private String name;
    private double radiant;

    public CircleShape(String name, double radiant) {
        this.name = name;
        this.radiant = radiant;
    }

    public String getName() {
        return name;
    }

    public double getRadiant() {
        return radiant;
    }

    public double Area() {
        double area = Math.PI * (radiant*radiant);
        return area;
    }

    public double Girth() {
        double girth = 2 * Math.PI * radiant;
        return girth;
    }

    public void print() {
        System.out.println(name + " Area = " + Area());
        System.out.println(name + " Girth = " + Girth());
    }
}
