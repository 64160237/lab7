package com.jtrps.week8;

public class TestMap1 {
    public int width;
    public int height;

    public TestMap1(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public TestMap1() {
        this.width = 5;
        this.height = 5;
    }

    public void print() {
        System.out.println("MAP 1");
        for (int i = 0; i < height; i++) { 
            for (int j = 0; j < width+1; j++) {
                if(j == width){
                    System.out.println();
                }else{
                    System.out.print("-");
                }
            }
        }
    }
}
